# KerbolExplorer
This repository contains scripts for the Kerbal Space Program mod [kOS](https://ksp-kos.github.io/KOS/). 

The code is developed to land a probe on Duna. The library code should work for other missions and rockets as well though.

## Dependencies
For the code to work correctly you will need:
* [kOS](https://ksp-kos.github.io/KOS/)
* [RemoteTech](https://github.com/RemoteTechnologiesGroup/RemoteTech)