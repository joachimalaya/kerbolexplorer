runoncepath("0:/lib/orbitManeuver.ks").

// takes an optional parameter for desired height of the orbit
// be advised, that targetting a high orbit is usually less efficient than getting to LKO and then raising the orbit
function ascendFromKSC {
    parameter targetHeight is 80000.
    print "PREPARING FOR LAUNCH".
    wait 10.

    lock throttle to 1.6 / (max(ship:availablethrustat(ship:sensors:pres / 100), 1) / (ship:mass * ship:sensors:grav:mag)).

    print "IGNITION".
    stage.
    lock steering to heading(90, 90 - min(1, apoapsis / (targetHeight * 0.9)) * 90).

    print "ASCENDING...".
    wait until apoapsis > targetHeight.

    unlock throttle.
    unlock steering.

    print "APOAPSIS AT LKO ALTITUDE".
    print "PREPARING CIRCULARIZATION".
    scheduleCircularizationAtApo().
    wait until ship:altitude > 71000.
    executeNextNode().

    print "ASCEND COMPLETE".
}