
function compareApproximate {
    parameter left, right.

    return abs(left - right) < right * 0.05.
}

function semiMajorAxis {
    return (apoapsis + periapsis + 2 * body:radius) / 2.
} 