// collection of functions for effective logging
// has initialization code; use run once

set logPaths to list().

function addToLogPaths {
    parameter logPath.

    if not logPaths:contains(logPath) {
        logPaths:add(logPath).
    }
}

function dumpLogs {
    for logPath in logPaths {
        movepath(logPath, "0:/" + logPath).
    }
}

function logToCSV {
    parameter values.
    parameter logPath.

    set line to values:join(",").
    log line to logPath.

    addToLogPaths(logPath).
}