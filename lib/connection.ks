function alignAntennaToTarget {
    parameter antennaName, target.

    set antenna to ship:partstagged(antennaName)[0].
    set rtModule to antenna:getmodule("ModuleRTAntenna").
    rtModule:setfield("target", target).
}