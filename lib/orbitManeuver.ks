// tools for changing the orbit of the active ship

runoncepath("0:/lib/math.ks").

function executeNextNode {
    set node to nextnode.

    lock burntime to node:deltav:mag / (max(ship:maxthrust, 1) / ship:mass).
    warpto(time:seconds + node:eta - burntime / 2 - 60).
    // TODO: factor in rotation speed and needed rotation?
    wait until node:eta - burntime / 2 - 59 < 0.
    lock steering to node:burnvector.

    wait until node:eta < burntime / 2.
    set totalDeltaV to node:deltav:mag.

    lock throttle to 1.

    wait until burntime < 2.
    lock throttle to 0.5.
    wait until burntime < 0.5.
    lock throttle to 0.1.
    wait until node:deltav:mag < totalDeltaV * 0.001.
    lock throttle to 0.

    // need to unlock all usages of node
    unlock burntime.
    unlock steering.
    remove node.
    wait 0.
}

function scheduleCircularizationAtApo {
    set circularization to node(time:seconds + eta:apoapsis, 0, 0, 0).
    add circularization.
    wait 0.

    until compareApproximate(circularization:orbit:periapsis, apoapsis) {
        set circularization:prograde to circularization:prograde + 0.1.
    }
}

function scheduleCircularizationAtPeri {
    set circularization to node(time:seconds + eta:periapsis, 0, 0, 0).
    add circularization.
    wait 0.

    set targetApoapsis to periapsis.
    until circularization:orbit:apoapsis > targetApoapsis {
        set circularization:prograde to circularization:prograde - 0.1.
    }
}

function changeOrbitHeight {
    parameter targetHeight.
    parameter transferTime is time:seconds + eta:apoapsis.

    if targetHeight < periapsis {
        set transferNode to node(transferTime, 0, 0, 0).
        add transferNode.
        wait 0.

        until transferNode:orbit:periapsis < targetHeight {
            set transferNode:prograde to transferNode:prograde - 0.1.
        }

        executeNextNode().
        scheduleCircularizationAtPeri().
        executeNextNode().
    } else if targetHeight > apoapsis {
        set transferNode to node(transferTime, 0, 0, 0).
        add transferNode.
        wait 0.

        until transferNode:orbit:apoapsis > targetHeight {
            set transferNode:prograde to transferNode:prograde + 0.1.
        }

        executeNextNode().
        scheduleCircularizationAtApo().
        executeNextNode().
    } else {
        print "changeOrbitHeight(" + targetHeight + ") NOT IMPLEMENTED FOR CURRENT CONDITIONS".
        // TODO: ???
    }
}

function fineTuneSemiMajorAxis {
    parameter targetSemiMajorAxis.

    if targetSemiMajorAxis > semiMajorAxis() {
        lock steering to prograde.
        lock waitCondition to targetSemiMajorAxis < semiMajorAxis().
    } else {
        lock steering to retrograde.
        lock waitCondition to targetSemiMajorAxis > semiMajorAxis().
    }

    // TODO: dynamic waiting
    wait 30.

    lock throttle to 0.01.

    wait until waitCondition.
}