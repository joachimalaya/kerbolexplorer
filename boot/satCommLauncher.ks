runoncepath("0:/lib/ascend.ks").
runoncepath("0:/lib/orbitManeuver.ks").
runoncepath("0:/lib/connection.ks").

function launchSatComm {
    parameter satNumber.

    ascendFromKSC().
    
    changeOrbitHeight(2863000, time:seconds + orbit:period * ((3 + satNumber) / 4)).
    fineTuneSemiMajorAxis(3463330).

    set originalName to shipname.
    set shipname to originalName + satNumber.

    toggle ag1.
    alignAntennaToTarget("KSCUplink", "Mission Control").
    alignAntennaToTarget("LongDistanceDish", "active-vessel").
    alignAntennaToTarget("CommNetUplink", "active-vessel").
    if satNumber > 0 {
        alignAntennaToTarget("CommNetDownlink", originalName + (satNumber - 1)).
    }

    toggle ag10.
    lock steering to up.
}

// set satNumber for each satellite
launchSatComm(0).