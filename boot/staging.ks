// boot this script on a cpu to enable automatic staging on engine flameout
print "this processor will trigger stages when engines burn out.".

list engines in engines.
until false {
    for engine in engines {
        if engine:flameout {
            print "engine flameout".
            if stage:ready {
                print "staging due to engine flameout".
                stage.
            }
        }
    }
    wait 1.
}
